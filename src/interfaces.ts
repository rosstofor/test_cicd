export interface UserAttributes {
  id?: number;
  first_name: string;
  last_name: string;
  email: string;
  avatar?: string | null;
  password: string;
}
